package com.epam.mvnproj.menu;

import com.epam.mvnproj.AbsTestLogged;
import com.epam.mvnproj.UI.UI;
import java.util.Map;
import static org.testng.AssertJUnit.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MainMenuTests extends AbsTestLogged {
  private Menu testMainMenu;

  @BeforeClass
  public void getSampleMenu() {
    testMainMenu = UI.newMainMenu();
    testLog.trace("Menu object created");
  }

  @Test
  public void whenMainMenuCreatedMapHasSize() {
    Map<String, Performable> newMenu = testMainMenu.prepareExeMenu();
    testLog.trace("Getting menu as map");
    assertEquals(4, newMenu.size());
  }

}
