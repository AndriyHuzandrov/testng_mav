package com.epam.mvnproj.model.dao;

import com.epam.mvnproj.AbsTestLogged;
import com.epam.mvnproj.model.District;
import java.sql.SQLException;
import java.util.Optional;
import static org.testng.AssertJUnit.*;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;


//@RunWith(MockitoJUnitRunner.class)
public class DistrictDaoTests extends AbsTestLogged {
  private DAO<District, Integer> data;
/*
  @InjectMocks
  DistrictDao data;
  @Mock
  Connection connection;
  @Mock
  PreparedStatement stmt;
  @BeforeAll
  public void setUp() throws SQLException {
    when(connection.prepareStatement(eq("INSERT INTO district (distr_name) VALUES (?)"))).thenReturn(stmt);
    when(stmt.executeUpdate()).thenReturn(1);
  }



  @Test
  public void testInsertNewDistrict() throws SQLException{
    Assertions.assertTrue(data.create(new District()));
  }
 */
  @AfterMethod
  public void closePool() {
    data.closeConnection();
    testLog.trace("connection pool closed");
  }
  @Test
  public void returnedListSizeSameAsDBTable() throws SQLException{
    data = new DistrictDao();
    testLog.trace("DAO created. Getting all the records");
    assertEquals(3, data.getAll().size());
  }

  @Test
  public void getRecordWhenNameIsSameThenOK() throws SQLException {
    data = new DistrictDao();
    testLog.trace("DAO created. Getting object by id");
    Optional<District> result = data.getById(2);
    assertEquals("Vinitska",
        result.orElseGet(() -> new District()).getDistrName());
  }

  @Test(expectedExceptions = SQLException.class)
  public void whenDoubleInsertionThenSQLException() throws SQLException {
    data = new DistrictDao();
    testLog.trace("DAO created. Inserting duplicate record");
    District testDistr = new District();
    testDistr.setDistrName("Sumska");
    data.create(testDistr);
  }

}
