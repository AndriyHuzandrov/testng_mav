package com.epam.mvnproj.reporting;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReportConsts {
   static final Logger reportLog = LogManager.getLogger("testing");
   static final String HORIZ_BAR = "\n--------------------------------------------------\n";

  private ReportConsts() {}

  static Logger getReportLog() {
    return reportLog;
  }
}
