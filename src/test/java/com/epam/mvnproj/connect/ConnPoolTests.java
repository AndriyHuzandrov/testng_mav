package com.epam.mvnproj.connect;

import static com.epam.mvnproj.txtconst.TxtConsts.CONF_FILENAME;
import static com.epam.mvnproj.txtconst.TxtConsts.FILE_ERR;

import com.epam.mvnproj.AbsTestLogged;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

public class ConnPoolTests extends AbsTestLogged {
  private Properties props;
  private Connectible cPool;

  @BeforeClass
  public void makeConString() {
    testLog.trace("Starting connection pool testing routine");
    props = new Properties();
    testLog.trace("Reading connection config file");
    try(FileInputStream fr = new FileInputStream(CONF_FILENAME)) {
      props.load(fr);
    } catch (IOException e) {
      testLog.error(FILE_ERR);
    }
    testLog.trace("Read successful");
  }

  @BeforeClass
  public void  openPool() throws SQLException {
    cPool = ConnectionDispatch.create(
        props.getProperty("url"),
        props.getProperty("user"),
        props.getProperty("paswd")
    );
    testLog.trace("Connection pool created");
  }

  @AfterClass
  public void closePool() throws SQLException {
    testLog.trace("Closing pool");
    cPool.shutdown();
  }

  @Test
  public void  whenExtractFromPoolAndIsAlive() throws SQLException {
    assertTrue(cPool.getConnection().isValid(1));
  }

  @Test(expectedExceptions = Exception.class)
  public void whenPoolClosedAndGetConnectionThenError() throws SQLException {
    testLog.trace("Generating exception while operating with closed pool");
    cPool.shutdown();
    cPool.getConnection();
  }



}
