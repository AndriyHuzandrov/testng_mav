package com.epam.mvnproj.connect;

import java.sql.Connection;
import java.sql.SQLException;

public interface Connectible {
  Connection getConnection();
  boolean releaseConnection(Connection c);
  void shutdown() throws SQLException;
}
