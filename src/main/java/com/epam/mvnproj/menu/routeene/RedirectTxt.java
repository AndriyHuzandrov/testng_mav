package com.epam.mvnproj.menu.routeene;

import static com.epam.mvnproj.txtconst.TxtConsts.LEAVE;

import java.util.LinkedList;
import java.util.List;

public class RedirectTxt extends Showable {

  List<String> fillMenu() {
    menu = new LinkedList<>();
    menu.add("1. Redirect this parcel.");
    menu.add(LEAVE);
    return menu;
  }

}
